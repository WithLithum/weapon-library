﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using GTA;
using AdvancedHookManaged;
using NativeFunctionHook;

namespace WeaponLibrary.API
{
    public class ArmedLibrary
    {
        Checkpoint check;
        Timer tickTimer;

        Blip armedLibraryBlip;

        

        private bool Eplased;
        private bool Restored;

        public ArmedLibrary(Vector3 position)
        {
            tickTimer = new Timer();
            tickTimer.Interval = 100;
            tickTimer.Tick += new EventHandler(tickTimer_Tick);

            check = new Checkpoint(position, Color.Red, 2);

            NWorld.DrawCheckpoint(check.Position, 2f, 255, 0, 0);

            tickTimer.Start();
        }

        void tickTimer_Tick(object sender, EventArgs e)
        {
            if (Game.LocalPlayer.Character.Position.DistanceTo(check.Position) <= 2f && !Eplased)
            {
                Eplased = true;
                Game.LocalPlayer.Character.Weapons.BasicShotgun.Ammo = 100;
                Game.LocalPlayer.Character.Weapons.BasicShotgun.AmmoInClip = 5;
            }
            else if (Game.LocalPlayer.Character.Position.DistanceTo(check.Position) <= 2f)
            {
                AGame.PrintText("This library was emptied.");
            }
            if (Eplased && !Restored && Game.LocalPlayer.Character.Position.DistanceTo(check.Position) >= 300f)
            {
                Eplased = false;
                Restored = true;
                AGame.PrintText("A Firearm Library at "+World.GetStreetName(check.Position) + " was opened");
            }
        }

        void check_CheckedAtCheckpoint()
        {

        }
    }
}
