﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using GTA;

namespace WeaponLibrary
{
    public class Main : Script
    {
        Weapon weap;

        API.ArmedLibrary library1;
        API.ArmedLibrary library2;
        public Main()
        {
            
            try
            {
                library1 = new API.ArmedLibrary(new Vector3(896.486f, -497.571f, 13.7546f));
                library2 = new API.ArmedLibrary(new Vector3(805.392f, -408.174f, 45.7211f));
            }
            catch (Exception ex)
            {
                Game.DisplayText("WeaponLib: " + ex.Message);
                System.IO.File.WriteAllText("WeaponLib_CurrentError.log", ex.ToString());
            }
        }
    }
}
